<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Handles Questions CRUD
Route::resource('questions', \App\Http\Controllers\QuestionsController::class)->except('show');
Route::get('questions/{slug}', [\App\Http\Controllers\QuestionsController::class, 'show']);

// Handles Mark as Best Answer Feature
Route::put('questions/{question}/answers/{answer}/best-answer', [\App\Http\Controllers\QuestionAnswersController::class, 'markAsBest'])->name('markAsBest');

// Handles Answers CRUD
Route::middleware(['auth'])->group(function() {
    Route::get('questions/{question}/answer/create', [\App\Http\Controllers\QuestionAnswersController::class, 'create'])->name('questionAnswer.create');
    Route::post('questions/{question}/answer', [\App\Http\Controllers\QuestionAnswersController::class, 'store'])->name('questionAnswer.store');
    Route::get('question/{question}/answer/{answer}/edit', [\App\Http\Controllers\QuestionAnswersController::class, 'edit'])->name('questionAnswer.edit');
    Route::put('question/{question}/answer/{answer}/update', [\App\Http\Controllers\QuestionAnswersController::class, 'update'])->name('questionAnswer.update');
    Route::delete('questions/{question}/answer/{answer}/delete', [\App\Http\Controllers\QuestionAnswersController::class, 'destroy'])->name('questionAnswer.destroy');
});

// Handles Mark As Fav & UnFav Question Feature
Route::post('questions/{question}/favorite', [\App\Http\Controllers\FavoritesController::class, 'store'])->name('questions.favorite');
Route::delete('questions/{question}/unfavorite', [\App\Http\Controllers\FavoritesController::class, 'destroy'])->name('questions.unfavorite');

// Handles Votes System for Question and Answers
Route::post('questions/{question}/vote/{vote}', [\App\Http\Controllers\VotesController::class, 'voteQuestion'])->name('questions.vote');
Route::post('answers/{answer}/vote/{vote}', [\App\Http\Controllers\VotesController::class, 'voteAnswer'])->name('answers.vote');


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
