@extends('frontend-layouts.app')

@section('page-level-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.css" />
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <h3 class="card-header">
                    Edit your Answer
                </h3>
                <div class="card-body">
                    <form action="{{ route('questionAnswer.update', [$question, $answer]) }}" method="POST">
                        @csrf
                        @method("PUT")
                        <div class="form-group">
                            <label for="body">Answer</label>
                            <input type="hidden"
                                class="form-control"
                                value="{{ old('body', $answer->body) }}"
                                name="body" id="body"/>
                            <trix-editor input="body"
                                        placeholder="Edit the Answer"
                                        class="form-control"></trix-editor>

                            @error('body')
                                    <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group mt-3">
                            <input type="submit" value="Submit Answer" class="btn btn-outline-success">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-level-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.js"></script>
@endsection
