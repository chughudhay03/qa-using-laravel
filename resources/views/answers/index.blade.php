<div class="row mt-4">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="mt-0">{{ \Illuminate\Support\Str::plural('Answer', $question->answers_count) }}</h3>
            </div>
            <div class="card-body">
                @foreach($question->answers as $answer)
                    {!! $answer->body !!}
                    <div class="d-flex justify-content-between mr-3 mb-3">
                        <div class="d-flex">
                            <div>
                                @auth
                                    <form action="{{ route('answers.vote', [$answer, 1]) }}" method="POST">
                                        @csrf
                                        <button type="submit"
                                                title="Vote Up"
                                                class="vote-up d-block text-center {{ $answer->hasMarkedUpVote(auth()->user()) ? ' text-success' : 'text-dark'}} border-0">
                                            <i class="fa fa-caret-up fa-3x"></i>
                                        </button>
                                    </form>
                                    <h4 class="m-0 text-muted text-center">{{ $answer->votes_count }}</h4>
                                    <form action="{{ route('answers.vote', [$answer, -1]) }}" method="POST">
                                        @csrf
                                        <button type="submit"
                                                title="Vote Down"
                                                class="vote-down d-block text-center {{ $answer->hasMarkedDownVote(auth()->user()) ? ' text-danger' : 'text-dark'}} border-0">
                                            <i class="fa fa-caret-down fa-3x"></i>
                                        </button>
                                    </form>
                                @else
                                    <a href="{{ route('login') }}" title="Vote Up" class="vote-up d-block text-center text-dark">
                                        <i class="fa fa-caret-up fa-3x"></i>
                                    </a>
                                    <h4 class="m-0 text-muted text-center">{{ $answer->votes_count }}</h4>
                                    <a href="{{ route('login') }}" title="Vote Down" class="vote-down d-block text-center text-dark">
                                        <i class="fa fa-caret-down fa-3x"></i>
                                    </a>
                                @endauth
                            </div>
                            <div class="mt-3 ms-3">
                                @can('markAsBest', $answer)
                                    <form action="{{route('markAsBest', ['question' => $question, 'answer'=>$answer])}}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <button title="Mark as Best"
                                                class="favorite d-block text-center mb-2 border-0"
                                                type="submit">
                                            <i class="fa fa-check fa-2x {{ $answer->isBest($question) ? 'text-success' :'text-dark' }}"></i>
                                        </button>
                                    </form>
                                @else
                                    @if($answer->isBest($question))
                                        <i class="fa fa-check fa-2x text-success"></i>
                                    @endif
                                @endcan
                            </div>
                        </div>
                        <div class="d-flex flex-column">
                            <div class="text-end">
                                Answered {{ $answer->created_date }}
                            </div>
                            <div class="d-flex mt-2">
                                <div>
                                    <img src="{{ $answer->author->avatar }}">
                                </div>
                                <div class="mt-2 ms-2">
                                    {{ $answer->author->name }}
                                </div>
                            </div>
                            <div class="d-flex flex-row mt-2">
                                @can('update', $answer)
                                    <a href="{{ route('questionAnswer.edit', [$question, $answer]) }}" class="btn btn-warning">Edit</a>
                                @endcan
                                @can('delete', [$answer, $question])
                                    <form action="{{ route('questionAnswer.destroy', [$question, $answer]) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger ms-3" type="submit">Delete</button>
                                    </form>
                                @endcan
                            </div>
                        </div>
                    </div>
                    <hr>
                @endforeach
            </div>
        </div>
    </div>
</div>
