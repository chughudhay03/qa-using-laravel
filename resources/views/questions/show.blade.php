@extends('frontend-layouts.app')

@section('page-level-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h1>{{ $question->title }}</h1>
                </div>
                <div class="card-body">
                    {!! $question->body !!}
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-between mr-3">
                        <div class="d-flex">
                            <div>
                                @auth
                                    <form action="{{ route('questions.vote', [$question, 1]) }}" method="POST">
                                        @csrf
                                        <button type="submit"
                                                title="Vote Up"
                                                class="vote-up d-block text-center {{ $question->hasMarkedUpVote(auth()->user()) ? ' text-success' : 'text-dark'}} border-0">
                                            <i class="fa fa-caret-up fa-3x"></i>
                                        </button>
                                    </form>
                                    <h4 class="m-0 text-muted text-center">{{ $question->votes_count }}</h4>
                                    <form action="{{ route('questions.vote', [$question, -1]) }}" method="POST">
                                        @csrf
                                        <button type="submit"
                                                title="Vote Down"
                                                class="vote-down d-block text-center {{ $question->hasMarkedDownVote(auth()->user()) ? ' text-danger' : 'text-dark'}} border-0">
                                            <i class="fa fa-caret-down fa-3x"></i>
                                        </button>
                                    </form>
                                @else
                                    <a href="{{ route('login') }}" title="Vote Up" class="vote-up d-block text-center text-dark">
                                        <i class="fa fa-caret-up fa-3x"></i>
                                    </a>
                                    <h4 class="m-0 text-muted">{{ $question->votes_count }}</h4>
                                    <a href="{{ route('login') }}" title="Vote Down" class="vote-down d-block text-center text-dark">
                                        <i class="fa fa-caret-down fa-3x"></i>
                                    </a>
                                @endauth
                            </div>
                            <div class="mt-3 ms-3">
                                @can('markAsFavorite', $question)
                                        @if($question->is_favorite)
                                            <form action="{{ route('questions.unfavorite', $question) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" title="Mark as Fav" class="favorite d-block text-center mb-2 border-0">
                                                    <i class="fa fa-star fa-2x text-warning"></i>
                                                </button>
                                            </form>
                                        @else
                                            <form action="{{ route('questions.favorite', $question) }}" method="POST">
                                                @csrf
                                                <button type="submit" title="Mark as Fav" class="favorite d-block text-center mb-2 border-0">
                                                    <i class="fa fa-star fa-2x text-dark"></i>
                                                </button>
                                            </form>
                                        @endif
                                        <h4 class="text-muted text-center">{{ $question->favorites_count }}</h4>
                                    @endcan
                            </div>
                        </div>
                        <div class="d-flex flex-column">
                            <div class="text-end">
                                Asked {{ $question->created_date }}
                            </div>
                            <div class="d-flex mt-2">
                                <div>
                                    <img src="{{ $question->owner->avatar }}">
                                </div>
                                <div class="mt-2 ms-2">
                                    {{ $question->owner->name }}
                                </div>
                            </div>
                            <div class="text-end mt-2">
                                <a href="{{ route('questionAnswer.create', $question) }}"
                                    class="btn btn-primary">
                                    Give an Answer
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('answers.index')
</div>
@endsection
