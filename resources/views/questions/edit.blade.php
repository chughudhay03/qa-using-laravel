@extends('frontend-layouts.app')

@section('page-level-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.css" />
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Ask a Question</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('questions.update', $question) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group mb-3">
                                <label for="title">Title</label>
                                <input type="text"
                                       class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}"
                                       value="{{ old('title', $question->title) }}"
                                       placeholder="Enter Question Title"
                                       name="title" id="title" />
                                @error('title')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label for="body">Question</label>
                                <input type="hidden"
                                       value="{{ old('body', $question->body) }}"
                                       class="form-control"
                                       name="body" id="body" />
                                <trix-editor input="body"
                                             placeholder="Ask your question"
                                             class="form-control {{ $errors->has('body') ? 'is-invalid' : '' }}">
                                </trix-editor>
                                @error('body')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <input type="submit" value="Ask Experts" class="btn btn-outline-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-level-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.js"></script>
@endsection
