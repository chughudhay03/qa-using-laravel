<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Answer;
use App\Models\Question;
use App\Models\User;
use Database\Factories\AnswerFactory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create()->each(function ($user) {
        //     for ($i = 1; $i <= random_int(2, 8); $i++) {
        //         $question = $user->questions()->create(Question::factory()->make()->toArray());
        //         for ($j = 1; $j <= random_int(5, 10); $j++) {
        //             $question->answers()->create(Answer::factory()->make()->toArray());
        //         }
        //         $question->answers()->saveMany(Answer::factory(random_int(5, 10))->make());
        //     }
        // }

        User::factory(10)->create()->each(function ($user) {
            $user->questions()
                ->saveMany(Question::factory(random_int(2, 8))->make())
                ->each(function ($question) {
                    $question->answers()->saveMany(Answer::factory(random_int(5, 10))->make());
                });
        });
    }
}
