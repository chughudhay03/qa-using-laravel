<?php

namespace App\Http\Controllers;

use App\Http\Requests\Questions\UpdateQuestionRequest;
use App\Http\Requests\Questions\CreateQuestionRequest;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class QuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth'])->except(['index', 'show']);
    }

    public function index()
    {
        $questions = Question::with('owner')->latest()->paginate(10);
        return view('questions.index', compact(['questions']));
    }

    public function create()
    {
        return view('questions.create');
    }

    public function store(CreateQuestionRequest $request)
    {
        //dd($request);
        auth()->user()->questions()->create([
            'title' => $request->title,
            'body' => $request->body
        ]);

        session()->flash('success', 'Question has been reported to all our expert users!');
        return redirect(route('questions.index'));
    }

    public function show(Question $question)
    {
        //dd($question);
        $question->increment('views_count');
        return view('questions.show', compact(['question']));
    }

    public function edit(Question $question)
    {
        if($this->authorize('update', $question)) {
            return view('questions.edit', compact(['question']));
        }
        return abort(403);
    }

    public function update(UpdateQuestionRequest $request, Question $question)
    {
        if($this->authorize('update', $question)) {
            $question->update([
                'title' => $request->title,
                'body' => $request->body
            ]);
            session()->flash('success', 'Question has been updated and will be answered accordingly soon!');
            return redirect(route('questions.index'));
        }
        return abort(403);
    }

    public function destroy(Question $question)
    {
        if($this->authorize('delete', $question)) {
            $question->delete();
            session()->flash('success', 'Question deleted! Why did you waste time by asking? :|');
            return redirect(route('questions.index'));
        }
        return abort(403);
    }
}
