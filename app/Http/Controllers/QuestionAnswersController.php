<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use Illuminate\Http\Request;

class QuestionAnswersController extends Controller
{
    public function markAsBest(Question $question, Answer $answer)
    {
        $this->authorize('markAsBest', $answer);
        $question->markAsBest($answer);
        return redirect()->back();
    }

    public function create(Question $question)
    {
        return view('answers.create', compact(['question']));
    }

    public function store(Request $request, Question $question)
    {
        auth()->user()->answers()->create([
            'question_id' => $question->id,
            'body' => $request->body
        ]);
        session()->flash('success', 'Answer has been reported to this question!');
        return redirect("/questions/{$question->slug}");
    }

    public function edit(Question $question, Answer $answer)
    {
        if($this->authorize('update', $answer))
        {
            return view('answers.edit', compact(['question', 'answer']));
        }
        return abort(403);
    }

    public function update(Request $request, Question $question, Answer $answer)
    {
        if($this->authorize('update', $answer))
        {
            $answer->update([
                'body' => $request->body
            ]);
            session()->flash('success', 'Answer has been Updated for this Question!');
            return redirect($question->url);
        }
        return abort(403);
    }

    public function destroy(Question $question, Answer $answer)
    {
        if($this->authorize('delete', [$answer, $question]))
        {
            $answer->delete();
            session()->flash('success', 'Answer has been Deleted for this Question!');
            return redirect($question->url);
        }
        return abort(403);
    }

}
