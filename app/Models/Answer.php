<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    use HasFactory;

    protected $guarded = [];

    public static function boot()
    {
        parent::boot();
        static::created(function($answer) {
            $answer->question->increment('answers_count');
        });
    }

    public function vote(int $vote)
    {
        $this->votes()->attach(auth()->id(), ['vote'=>$vote]);
        if($vote > 0) {
            $this->increment('votes_count');
        } else {
            $this->decrement('votes_count');
        }
    }

    public function updateVote(int $vote)
    {
        $this->votes()->updateExistingPivot(auth()->id(), ['vote'=> $vote]);
        if($vote > 0) {
            $this->increment('votes_count');
            $this->increment('votes_count');
        } else {
            $this->decrement('votes_count');
            $this->decrement('votes_count');
        }
    }

    public function hasMarkedUpVote(User $user)
    {
        return $this->votes()->where('user_id', $user->id)->where('vote', 1)->exists();
    }

    public function hasMarkedDownVote(User $user)
    {
        return $this->votes()->where('user_id', $user->id)->where('vote', -1)->exists();
    }

    public function hasVoted(User $user)
    {
        return $this->votes()->where('user_id', $user->id)->exists();
    }

    public function isBest(Question $question)
    {
        return $question->best_answer_id === $this->id;
    }
    
    public function getCreatedDateAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    /**
     * Relationship Methods
     */

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function votes()
    {
        return $this->morphToMany(User::class, 'vote')->withTimestamps();
    }

}
