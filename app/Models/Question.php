<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Question extends Model
{
    protected $guarded = [];

    use HasFactory;

    /**
     * Mutators
     */

    public function setTitleAttribute($title){
        $this->attributes['title'] = $title;
        $this->attributes['slug'] = Str::slug($title);
    }

    /**
     * Accessors
     */

    public function getUrlAttribute() {
        return "questions/$this->slug";
    }

    public function getCreatedDateAttribute() {
        return $this->created_at->diffForHumans();
    }

    public function getStylesForAnswerAttribute(){
        if($this->answers_count > 0){
            if($this->best_answer_id){
                return "has-best-answer";
            }   else    {
                return "answered";
            }
        }
        return "unanswered";
    }

    public function getIsFavoriteAttribute()
    {
        return $this->favorites()->where(['user_id'=> auth()->id()])->count() > 0;
    }

    public function getFavoritesCountAttribute()
    {
        return $this->favorites->count();
    }

    /**
     *
     */
    public function markAsBest(Answer $answer)
    {
        $this->best_answer_id = $answer->id;
        $this->save();
    }

    public function vote(int $vote)
    {
        $this->votes()->attach(auth()->id(), ['vote'=>$vote]);
        if($vote > 0) {
            $this->increment('votes_count');
        } else {
            $this->decrement('votes_count');
        }
    }

    public function updateVote(int $vote)
    {
        $this->votes()->updateExistingPivot(auth()->id(), ['vote'=> $vote]);
        if($vote > 0) {
            $this->increment('votes_count');
            $this->increment('votes_count');
        } else {
            $this->decrement('votes_count');
            $this->decrement('votes_count');
        }
    }

    public function hasMarkedUpVote(User $user)
    {
        return $this->votes()->where('user_id', $user->id)->where('vote', 1)->exists();
    }

    public function hasMarkedDownVote(User $user)
    {
        return $this->votes()->where('user_id', $user->id)->where('vote', -1)->exists();
    }

    public function hasVoted(User $user)
    {
        return $this->votes()->where('user_id', $user->id)->exists();
    }

    /**
     * Relationship Methods
     */

    public function owner(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function favorites()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function votes()
    {
        return $this->morphToMany(User::class, 'vote')->withTimestamps();
    }

}
